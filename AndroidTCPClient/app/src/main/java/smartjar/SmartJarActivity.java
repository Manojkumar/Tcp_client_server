package smartjar;


import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import ro.kazy.tcpclient.R;
import ro.kazy.tcpclient.TcpClient;

public class SmartJarActivity extends BaseActivity implements View.OnClickListener {

    Calendar cal;
    private TcpClient mTcpClient;
    private byte[] send_data = new byte[8];
    String id = "10:12:13:14:15";
    TextView tvResponse;
    char[] mMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smart_jar);

        cal = Calendar.getInstance();

        Button btSyncTime = (Button) findViewById(R.id.btSyncTime);
        Button btJarPair = (Button) findViewById(R.id.btJarPair);
        Button btjarUnpair = (Button) findViewById(R.id.btjarUnpair);
        Button btCalibrate = (Button) findViewById(R.id.btCalibrate);
        Button btjarData = (Button) findViewById(R.id.btjarData);
        Button btProcessOP = (Button) findViewById(R.id.btProcessOP);
        tvResponse = (TextView) findViewById(R.id.tvResponse);

        btSyncTime.setOnClickListener(this);
        btJarPair.setOnClickListener(this);
        btjarUnpair.setOnClickListener(this);
        btCalibrate.setOnClickListener(this);
        btjarData.setOnClickListener(this);
        btProcessOP.setOnClickListener(this);

        new ConnectTask().execute("");

        //processOutput(new byte[]{N_43,N_11,N_01,N_00});
    }


    private void parseJarData(byte[] result) {

        int qty_in_gms = combineThreeBytes(result[8], result[7], result[6]);
        byte battery = result[9];
        byte health = result[10];
        String time = (int)result[3]+":"+(int)result[4]+":"+(int)result[5];

        String msg =
                "Battery -" + (int) battery + " QTY - " + qty_in_gms
                        + " Health: " + (int) health+" Time: "+time;

        toastLong(msg);
        sop(msg);

        printHealthDesc(health);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mTcpClient != null) {
            // disconnect
            mTcpClient.stopClient();
            mTcpClient = null;
        }
    }

    public void syncTime() {
        int hr = cal.get(Calendar.HOUR_OF_DAY);
        int min = cal.get(Calendar.MINUTE);
        int sec = cal.get(Calendar.SECOND);

        String text = hr+":"+min+":"+sec+"\n"
                +intToByte(hr)+":"+intToByte(min)+":"+intToByte(sec);
        sop(text);

        send_data = new byte[8];
        send_data[0] = N_52;
        send_data[1] = N_11;
        send_data[2] = N_03;
        send_data[3] = intToByte(hr);
        send_data[4] = intToByte(min);
        send_data[5] = intToByte(sec);
        send_data[6] = N_00;
        send_data[7] = N_00;

        //sends the message to the server
        if (mTcpClient != null) {
               mTcpClient.sendMessage(send_data);
        }

    }


    private void pairJar() {

        String[] split = id.split(":");

        try {
            if (split.length == 5) {
                send_data = new byte[8];

                send_data[0] = N_52;
                send_data[1] = N_10;
                send_data[2] = N_05;
                send_data[3] = (byte)Integer.parseInt(split[3]);
                send_data[4] = (byte)Integer.parseInt(split[2]);
                send_data[5] = (byte)Integer.parseInt(split[1]);
                send_data[6] = (byte)Integer.parseInt(split[0]);
                send_data[7] = (byte)Integer.parseInt(split[4]);

                //sends the message to the server
                if (mTcpClient != null) {
                    mTcpClient.sendMessage(send_data);
                }

            }else {
                toastShort("Invalid Hardware ID");
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void unPairJar() {


        String[] split = id.split(":");

        try {
            if (split.length == 5) {
                send_data = new byte[8];

                send_data[0] = N_52;
                send_data[1] = N_13;
                send_data[2] = N_05;
                send_data[3] = (byte)Integer.parseInt(split[3]);
                send_data[4] = (byte)Integer.parseInt(split[2]);
                send_data[5] = (byte)Integer.parseInt(split[1]);
                send_data[6] = (byte)Integer.parseInt(split[0]);
                send_data[7] = (byte)Integer.parseInt(split[4]);

                //sends the message to the server
                if (mTcpClient != null) {
                    mTcpClient.sendMessage(send_data);
                }

            }else {
                toastShort("Invalid Hardware ID");
            }

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void calibrate() {
        String[] split = id.split(":");

        try {
            if (split.length == 5) {
                send_data = new byte[8];

                send_data[0] = N_52;
                send_data[1] = N_14;
                send_data[2] = N_05;
                send_data[3] = (byte)Integer.parseInt(split[3]);
                send_data[4] = (byte)Integer.parseInt(split[2]);
                send_data[5] = (byte)Integer.parseInt(split[1]);
                send_data[6] = (byte)Integer.parseInt(split[0]);
                send_data[7] = (byte)Integer.parseInt(split[4]);

                //sends the message to the server
                if (mTcpClient != null) {
                    mTcpClient.sendMessage(send_data);
                }

            }else {
                toastShort("Invalid Hardware ID");
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void getJarData() {

        String[] split = id.split(":");

        try {
            if (split.length == 5) {
                send_data = new byte[8];

                send_data[0] = N_52;
                send_data[1] = N_12;
                send_data[2] = N_05;
                send_data[3] = (byte)Integer.parseInt(split[3]);
                send_data[4] = (byte)Integer.parseInt(split[2]);
                send_data[5] = (byte)Integer.parseInt(split[1]);
                send_data[6] = (byte)Integer.parseInt(split[0]);
                send_data[7] = (byte)Integer.parseInt(split[4]);

                //sends the message to the server
                if (mTcpClient != null) {
                    mTcpClient.sendMessage(send_data);
                }

            }else {
                toastShort("Invalid Hardware ID");
            }

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btSyncTime:
                syncTime();
                break;
            case R.id.btJarPair:
                pairJar();
                break;
            case R.id.btjarUnpair:
                unPairJar();
                break;
            case R.id.btCalibrate:
                calibrate();
                break;
            case R.id.btjarData:
                getJarData();
                break;
            case R.id.btProcessOP:
                processOutput(new byte[]{0x69,0x12,0x02,0x0C,0x12,0x00,0x32,0x31,0x35,0x34,0x00});
                break;
        }
    }



    private void processOutput(byte[] result) {

        //toastShort(Arrays.toString(result));

      /*  String str = "";
        for(int i=0;i<result.length;i++)
        {
            str+= Byte.toString(result[i]);
        }

        toastShort(str);*/
        Toast.makeText(getApplicationContext(),String.valueOf((int)(result[0])),Toast.LENGTH_LONG).show();

        int i = 0;

        if (result[i] == N_43) {
            //CFM
            switch (result[i + 1]) {
                case N_11:
                    //Sync RTC
                    parseOutput(result[i + 2], result[i + 3]);
                    break;
                case N_13:
                    //Un-Jar paring
                    parseOutput(result[i + 2], result[i + 3]);
                    break;
                case N_10:
                    //Jar paring
                    parseOutput(result[i + 2], result[i + 3]);
                    break;
                case N_14:
                    //Calibrate
                    parseOutput(result[i + 2], result[i + 3]);
                    break;
                case N_12:
                    //Get jar Data
                    parseOutput(result[i + 2], result[i + 3]);
                    break;
            }

        }else  if (result[i] == N_69) {
            //IND
            switch (result[i + 1]) {
                case N_12:
                    //Get jar Data
                    parseJarData(result);
                    break;
            }
        }

    }


    private void printHealthDesc(byte health) {

        if (health == N_00) {
            toastShort("All Modules are working as expected");
        }else if (health == N_01) {
            toastShort("Error in ADXL345 Module: No LP Mode possible");
        }else if (health == N_02) {
            toastShort("Error in Hx711 Module");
        }else if (health == N_04) {
            toastShort("Error in RFM Module (Cannot be reported)");
        }else if (health == N_08) {
            toastShort("Error in Battery Status Module (ADC conversion error)");
        }
    }

    private void parseOutput(byte b, byte b1) {
        if (b == N_01) {

            switch (b1) {
                case N_00:
                    //Success
                    toastShort("Success");
                    break;
                case N_01:
                    //Busy - Retry once
                    toastShort("Busy");
                    break;
                case N_02:
                    //Invalid Command
                    toastShort("Invalid Command");
                    break;
            }
        }
    }

    public class ConnectTask extends AsyncTask<String, String, TcpClient> {

        @Override
        protected TcpClient doInBackground(String... message) {

            //we create a TCPClient object and
            mTcpClient = new TcpClient(SmartJarActivity.this,new TcpClient.OnMessageReceived() {
                @Override
                public void messageReceived(char[] message) {
                    //receive here
                    mMessage =  message;

                    String temp_data=new String();
                    for(int i=0;i<4;i++)
                        temp_data+=String.valueOf((int)(message[i]));
                    publishProgress(temp_data);

                }


            });
            mTcpClient.run();

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            //Main thread - process here
            Toast.makeText(SmartJarActivity.this, values[0], Toast.LENGTH_SHORT).show();
            final char[] chars = values[0].toCharArray();
            byte[] result=new byte[4];
            for(int i=0;i<4;i++)
                result[i]=(byte)(mMessage[i]);

            //result = values[0].getBytes();
            processOutput(result);

        }
    }

}
