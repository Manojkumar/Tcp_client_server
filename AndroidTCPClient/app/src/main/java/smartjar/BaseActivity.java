package smartjar;

import android.app.Activity;
import android.widget.Toast;

/**
 * Created by balavishnu on 25/03/17.
 */

public class BaseActivity extends Activity {

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    final byte N_52 = 0x52, N_43 = 0x43, N_11 = 0x11,
            N_10 = 0x10,  N_13 = 0x13,N_14 = 0x14,N_12 = 0x12, N_04 = 0x04, N_08 = 0x08,
            N_05 = 0x05
            ,N_69 = 0x69,
             N_03 = 0x03
            ,N_00 = 0x00
            ,N_01 = 0x01
            ,N_02 = 0x02;

    final byte N_255 = (byte) 0xFF;

    public static String bytesToHex(byte[] bytes, int len) {
        char[] hexChars = new char[len * 2];
        for (int j = 0; j < len; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public byte intToByte(int data){
        return (byte) (data%256);
    }

    public void toastShort(String msg){
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    public void toastLong(String msg){
        Toast.makeText(this,msg,Toast.LENGTH_LONG).show();
    }


    public void sop(String msg){
        System.out.println(msg);
    }


    public int combineTwoBytes(byte highByte, byte lowByte) {
        int high = highByte >= 0 ? highByte : 256 + highByte;
        int low = lowByte >= 0 ? lowByte : 256 + lowByte;

        return low | (high << 8);
    }

    public int combineThreeBytes( byte lowByte, byte second, byte thirdByte) {

        int third = lowByte +(second *256) +(thirdByte * 65536);

        return third;
    }

}
