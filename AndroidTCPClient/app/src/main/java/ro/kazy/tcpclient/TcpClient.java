package ro.kazy.tcpclient;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Description
 *
 * @author Catalin Prata
 *         Date: 2/12/13
 */
public class TcpClient {

    public static final String SERVER_IP = "192.168.4.1"; //your computer IP address
    public static final int SERVER_PORT = 23;
    // message to send to the server
    //private String mServerMessage;
    // sends message received notifications
    private OnMessageReceived mMessageListener = null;
    // while this is true, the server will continue running
    private boolean mRun = false;
    // used to send messages
    private PrintWriter mBufferOut;
    // used to read messages from the server
    private BufferedReader mBufferIn;
    Activity c;
    char[] buffer = new char[2048];
    /**
     * Constructor of the class. OnMessagedReceived listens for the messages received from server
     */
    public TcpClient(Activity c, OnMessageReceived listener) {
        this.c = c;
        mMessageListener = listener;
    }

    /**
     * Sends the message entered by client to the server
     *
     * @param message text entered by client
     */
    public void sendMessage(String message) {
        if (mBufferOut != null && !mBufferOut.checkError()) {
            mBufferOut.println(message);
            mBufferOut.flush();
        }
    }

    public void sendMessage(byte[] send_data) {
        if (mBufferOut != null && !mBufferOut.checkError()) {
            toastShort("Sending "+send_data.toString());
            //mBufferOut.println(send_data);
            String text = null;
            try {
                text = new String(send_data, "UTF-8");
                char[] chars = text.toCharArray();
                mBufferOut.write(chars,0, send_data.length);
                mBufferOut.flush();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                toastShort(e.getMessage());
            }

        }else {
            toastShort("Failed to send");
        }
    }

    /**
     * Close the connection and release the members
     */
    public void stopClient() {

        // send mesage that we are closing the connection
        //sendMessage(Constants.CLOSED_CONNECTION + "Kazy");
        toastShort("Disconnecting..");

        mRun = false;

        if (mBufferOut != null) {
            mBufferOut.flush();
            mBufferOut.close();
        }

        mMessageListener = null;
        mBufferIn = null;
        mBufferOut = null;
        //mServerMessage = null;
    }

    public void run() {

        mRun = true;

        try {
            //here you must put your computer's IP address.
            InetAddress serverAddr = InetAddress.getByName(SERVER_IP);

            toastShort("C: Connecting..."+SERVER_IP+":"+SERVER_PORT);

            Log.e("TCP Client", "C: Connecting..."+SERVER_IP+":"+SERVER_PORT);

            //create a socket to make the connection with the server
            Socket socket = new Socket(serverAddr, SERVER_PORT);

            try {

                //sends the message to the server
                mBufferOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);

                //receives the message which the server sends back
                mBufferIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                // send login name
                //sendMessage(Constants.LOGIN_NAME + PreferencesManager.getInstance().getUserName());

                //in this while the client listens for the messages sent by the server
                while (mRun) {

                    //mServerMessage = mBufferIn.readLine();
                    int nread = mBufferIn.read(buffer, 0, buffer.length);

                    if (nread != -1 && mMessageListener != null) {
                        String temp_recv_data=new String();
                        for(int i=0;i<nread;i++)
                            temp_recv_data+=String.valueOf(buffer[i])+" ";

                        toastShort(temp_recv_data);

                        //call the method messageReceived from MyActivity class
                        mMessageListener.messageReceived(buffer);

                        //mMessageListener.messageReceived(mServerMessage);
                    }else {
                        toastShort("Unexpected end of data");
                    }

                }

                //Log.e("RESPONSE FROM SERVER", "S: Received Message: '" + mServerMessage + "'");

            } catch (final Exception e) {
                e.printStackTrace();
                Log.e("TCP", "S: Error", e);
                c.runOnUiThread(new Runnable() {
                    public void run() {
                        toastShort(e.getMessage());
                    }
                });



            } finally {
                //the socket must be closed. It is not possible to reconnect to this socket
                // after it is closed, which means a new socket instance has to be created.
                socket.close();
            }

        } catch (final Exception e) {
            e.printStackTrace();
            Log.e("TCP", "C: Error", e);
            c.runOnUiThread(new Runnable() {
                public void run() {
                    toastShort(e.getMessage());
                }
            });


        }

    }


    void toastShort(final String msg){
        new Thread()
        {
            public void run()
            {
                c.runOnUiThread(new Runnable()
                {
                    public void run()
                    {
                        //Do your UI operations like dialog opening or Toast here
                        Toast.makeText(c,msg,Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }.start();

    }

    //Declare the interface. The method messageReceived(String message) will must be implemented in the MyActivity
    //class at on asynckTask doInBackground
    public interface OnMessageReceived {
        public void messageReceived(char[] message);
        //public void messageReceived(String message);
    }
}
